﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Windows;

using LeafyRequestBot.Helpers;
using LeafyRequestBot.Windows;

using Serilog;

using Squirrel;

namespace LeafyRequestBot
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    internal sealed partial class App
    {
        private void AppStart(object sender, StartupEventArgs args)
        {
            Logger.CreateLogger();

            AppDomain.CurrentDomain.UnhandledException +=
                (s, e) => Logger.FatalException((Exception) e.ExceptionObject);

#if !DEBUG
            CheckUpdates();
#endif

            new MainWindow().Show();
        }

        // ReSharper disable once UnusedMember.Local
        private static async void CheckUpdates()
        {
            Log.Information("Checking for updates...");
            using (var mgr = new UpdateManager("https://g.whaskell.pw/requestbot/releases"))
            {
                await mgr.UpdateApp();
            }
        }
    }
}