﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

namespace LeafyRequestBot.ViewModels
{
    /// <summary>
    ///     Interaction logic for Requests.xaml
    /// </summary>
    internal sealed partial class Requests
    {
        public Requests()
        {
            InitializeComponent();
        }
    }
}