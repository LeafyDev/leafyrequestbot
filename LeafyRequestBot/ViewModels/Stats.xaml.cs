﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

namespace LeafyRequestBot.ViewModels
{
    /// <summary>
    ///     Interaction logic for Stats.xaml
    /// </summary>
    internal sealed partial class Stats
    {
        public Stats()
        {
            InitializeComponent();
        }
    }
}