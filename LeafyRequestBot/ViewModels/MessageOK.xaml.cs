﻿// ---------------------------------------------------------
// Copyrights (c) 2018 Plutonium All rights reserved.
// ---------------------------------------------------------

namespace LeafyRequestBot.ViewModels
{
    /// <summary>
    ///     Interaction logic for Login.xaml
    /// </summary>
    internal sealed partial class MessageOK
    {
        public MessageOK()
        {
            InitializeComponent();
        }

        // ReSharper disable MemberCanBePrivate.Global
        public static string Title { get; set; }
        public static string Message { get; set; }
        public static int MessageWidth { get; set; }
        public static int MessageHeight { get; set; }
        // ReSharper restore MemberCanBePrivate.Global

        public static void Define(string title, string message, int width = 250, int height = 150)
        {
            Title = title;
            Message = message;
            MessageWidth = width;
            MessageHeight = height;
        }
    }
}