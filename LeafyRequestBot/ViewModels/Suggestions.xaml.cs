﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

namespace LeafyRequestBot.ViewModels
{
    /// <summary>
    ///     Interaction logic for Suggestions.xaml
    /// </summary>
    public partial class Suggestions
    {
        public Suggestions()
        {
            InitializeComponent();
        }
    }
}