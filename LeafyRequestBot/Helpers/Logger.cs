﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

using DiscordWebhook;

using PastebinAPI;

using Serilog;
using Serilog.Events;

namespace LeafyRequestBot.Helpers
{
    internal static class Logger
    {
        public static void CreateLogger()
        {
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");

            Log.Logger = new LoggerConfiguration().WriteTo
                .File("logs/log-.txt", LogEventLevel.Verbose,
                    "({Timestamp:dd-MM @ hh:mm:ss}) [{Level:u3}] {Message}{NewLine}",
                    rollingInterval: RollingInterval.Day).MinimumLevel.Verbose().CreateLogger();

            Log.Information("Leafy bot started.");
        }

        public static async void FatalException(Exception ex)
        {
            Log.Fatal($"{ex.GetType()}: {ex.Message}");

            var url = "Error occurred during pasting.";

            try
            {
                Pastebin.DevKey = Config.PasteKey;
                var newPaste = await Paste.CreateAsync(ex.ToString(),
                                   $"LeafyBot error - {Config.Channel} @ {DateTime.Now:F}", Language.None,
                                   Visibility.Public, Expiration.Never);

                if(newPaste.Url.StartsWith("https://" + "pastebin", StringComparison.Ordinal))
                    url = newPaste.Url;
            }
            catch
            {
                // ignored
            }

            var embeds = new List<Embed>
            {
                new Embed
                {
                    Color = 16711680,
                    Footer = new EmbedFooter
                    {
                        IconUrl = "https://g.whaskell.pw/requestbot/leaf.png",
                        Text = DateTime.Now.ToString("ddd, dd MMM yyyy HH:mm:ss 'GMT' zz")
                    },
                    Fields = new List<EmbedField>
                    {
                        new EmbedField
                        {
                            Name = "App",
                            Value = "Leafy Request Bot",
                            Inline = true
                        },
                        new EmbedField
                        {
                            Name = "Type",
                            Value = ex.GetType().ToString(),
                            Inline = true
                        },
                        new EmbedField
                        {
                            Name = "Message",
                            Value = ex.Message
                        },
                        new EmbedField
                        {
                            Name = "Exception",
                            Value = url
                        },
                        new EmbedField
                        {
                            Name = "Channel Name",
                            Value = Config.Channel,
                            Inline = true
                        },
                        new EmbedField
                        {
                            Name = "Version",
                            Value =
                                FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion,
                            Inline = true
                        }
                    }
                }
            };

            await new Webhook(Config.DiscordID, Config.DiscordToken){ Embeds = embeds }.Send();
        }
    }
}