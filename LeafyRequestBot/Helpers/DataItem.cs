﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

namespace LeafyRequestBot.Helpers
{
    internal sealed class DataItem
    {
        public string TabName { get; set; }

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public object Content { get; set; }
    }
}