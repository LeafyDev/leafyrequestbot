﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LeafyRequestBot.Windows
{
    internal partial class MainWindow
    {
        private void Settings_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Connect_OnClick(object sender, RoutedEventArgs e)
        {
            if (ConnectVisibility == Visibility.Visible)
            {
                ConnectVisibility = Visibility.Collapsed;
                DisconnectVisibility = Visibility.Visible;
            }
            else
            {
                ConnectVisibility = Visibility.Visible;
                DisconnectVisibility = Visibility.Collapsed;
            }
        }

        private void ListBox_OnButtonUp(object sender, MouseButtonEventArgs e) => MenuToggleButton.IsChecked = false;

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (ItemsListBox.SelectedIndex)
            {
                case 0:
                    ActivePage = "Requests";
                    break;
                case 1:
                    ActivePage = "Suggestions";
                    break;
                case 2:
                    ActivePage = "Statistics";
                    break;
            }
        }
    }
}