﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LeafyRequestBot.Windows
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    internal sealed partial class MainWindow
    {
        public MainWindow()
        {
            ConnectVisibility = Visibility.Visible;
            DisconnectVisibility = Visibility.Collapsed;

            InitializeComponent();
        }
    }
}