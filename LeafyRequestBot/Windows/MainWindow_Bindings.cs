﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2018 LeafyDev 🍂 All rights reserved.
// ---------------------------------------------------------

using System.ComponentModel;
using System.Windows;

namespace LeafyRequestBot.Windows
{
    internal partial class MainWindow : INotifyPropertyChanged
    {
        private string _activePage;

        private Visibility _connectVisibility;
        private Visibility _disconnectVisibility;

        public string ActivePage
        {
            get => _activePage;
            set
            {
                _activePage = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ActivePage"));
            }
        }

        public Visibility ConnectVisibility
        {
            get => _connectVisibility;
            set
            {
                _connectVisibility = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ConnectVisibility"));
            }
        }

        public Visibility DisconnectVisibility
        {
            get => _disconnectVisibility;
            set
            {
                _disconnectVisibility = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DisconnectVisibility"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}